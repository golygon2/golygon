def construct_golygons(c):
    for i in c :
        max_edge_length = i[0]
        blocked = i[1]
        golygons = []
        def backtrack(x, y, edge_length, direction, j):
            if (x, y) in blocked or edge_length > max_edge_length:
                return
            if x == 0 and y == 0:
                golygons.append(j)
                return
            if direction != "s":
                backtrack(x, y+1, edge_length+1, golygon+"n")
            if direction != "n":
                backtrack(x, y-1, edge_length+1,golygon+"s")
            if direction != "w":
                backtrack(x+1, y, edge_length+1, golygon+"e")
            if direction != "e":
                backtrack(x-1, y, edge_length+1, golygon+"w")
        backtrack(0, 0, 1, "", "")
        for j in golygons:
            print(j)
        print("Found {} golygon(s).".format(len(golygons)))
c = [
    (8,{(-2, 0), (6, -2)}),
    (8,{(2, 1), (-2, 0)})
]

construct_golygons(c)

